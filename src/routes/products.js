const { router } = require("../exports/exports")
const apiAdapter = require("./apiAdapter")

const BASE_URL = "go:"
const api = apiAdapter(BASE_URL)

router.get("/api/getProducts", (req, res) => {
  api
    .get(req.path)
    .then(resp => {
      res.send(resp.data)
    })
    .catch(err => console.log(err))
})

router.post("/api/addProducts", (req, res) => {
  api
    .post(req.path, req.body)
    .then(resp => {
      res.send(resp.data)
    })
    .catch(err => console.log(err))
})

module.exports = router
