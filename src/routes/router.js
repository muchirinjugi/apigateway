const express = require("express")
const router = express.Router()
const products = require("./products")

router.use((req, res, next) => {
  console.log("called", req.path)
  next()
})

router.use(products)

module.exports = router
