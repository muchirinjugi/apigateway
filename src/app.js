/* eslint-disable security/detect-non-literal-fs-filename */
const fs = require("fs")
const path = require("path")
const http = require("http")
const https = require("https")

const { app } = require("./exports/exports")
const config = require("./environment/config")
const router = require("./routes/router")

app.get("/", (req, res) => {
  res.status(200).json({
    message: "welcome to green point recyclers api gateway it's working"
  })
})

app.use(router)

// Getting the key and cert from the keys dir
const key = fs.readFileSync(path.resolve(__dirname, "./ssl/key.pem"))
const cert = fs.readFileSync(path.resolve(__dirname, "./ssl/cert.pem"))

// Https server options
const tlsOptions = { key, cert }

// create a http2 server
const httpServer = http.createServer(app)

// start the http2 server
httpServer.listen(config.httpPort, function() {
  console.log(
    `http api gateway is listening on port ${config.httpPort} in ${
      config.envName
    } mode`
  )
})

// Create a https server
const httpsServer = https.createServer(tlsOptions, app)

// start the https server
httpsServer.listen(config.httpsPort, function() {
  console.log(
    `https apigateway is listening on port ${config.httpsPort} in ${
      config.envName
    } mode`
  )
})
