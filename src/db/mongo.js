const mongoose = require("mongoose")
const path = require("path")

require("dotenv").config({
  path: path.resolve(__dirname, "../environment/env.env")
})

mongoose.connect(process.env.MLAB_Credentials, { useNewUrlParser: true })
