/**
 * @author Muchiri njugi
 * @license MIT
 */

// Imports
const express = require("express")
const bodyParser = require("body-parser")
const helment = require("helmet")

// Initialize an instance of express and an instance of router
const app = express()
const router = express.Router()

// add bodyParser which we use to parse the body of our requests
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

// add helmet to our express app
app.use(helment())

// Export app and router
module.exports = { app, router }
